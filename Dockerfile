FROM openjdk:22-ea-10-jdk-bullseye

MAINTAINER Georges KEMAYO

RUN mkdir -p /opt/app/ssl 

WORKDIR /opt/app

EXPOSE 8443

COPY src/main/resources/certs/admctl.* /opt/app/ssl/admctl.*
COPY src/main/resources/certs/ca.* /opt/app/ssl/ca.*

COPY ./target/kubernetes-sample-admissioncontroller-*-SNAPSHOT.jar ./sample-admissioncontroller.jar

CMD ["-jar", "-Dspring.profiles.active=prod", "/opt/app/sample-admissioncontroller.jar"]

ENTRYPOINT ["java"]
