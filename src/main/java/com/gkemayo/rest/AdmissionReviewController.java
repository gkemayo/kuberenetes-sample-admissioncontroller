package com.gkemayo.rest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
public class AdmissionReviewController {
	
	Logger logger = LoggerFactory.getLogger(AdmissionReviewController.class);
	
	/**
	 * This controller ensure that the annotation <b>author</b> is provided 
	 * in a Deployment to be created. It is thus mandatory.
	 * 
	 * @param rootRequest
	 * @return
	 */
	@PostMapping("/validate")
	public ObjectNode validateDeploymentRequestToKubeApiServer(@RequestBody ObjectNode rootRequest) {
		
		logger.info("Begining call of AdmissionController /validate endpoint");
		ObjectNode jsonResultNode;
		ObjectMapper mapper = new ObjectMapper();
		jsonResultNode = mapper.createObjectNode();
		try {
			JsonNode jsonannotationNode = rootRequest.path("metadata").path("annotations");
			boolean allowed = false;
			if(jsonannotationNode.get("author") != null && StringUtils.isNotEmpty(jsonannotationNode.get("author").asText())) {
				allowed = true;
				logger.info("AdmissionController allow the request");
			}
						
			buildJsonResultNode(rootRequest, jsonResultNode, allowed);
		} catch (Exception e) {
			buildJsonResultNode(rootRequest, jsonResultNode, false);
		}
		logger.info("Ending call of AdmissionController /validate endpoint");
				
		return jsonResultNode;
	}

	/**
	 * Build Json Result Node passed as parameter
	 * 
	 * @param rootRequest
	 * @param jsonResultNode
	 * @param allowed
	 */
	private void buildJsonResultNode(ObjectNode rootRequest, ObjectNode jsonResultNode, boolean allowed) {
		jsonResultNode.put("apiVersion", "admission.k8s.io/v1");
		jsonResultNode.put("kind", "AdmissionReview");	
		
		JsonNode jsonmetadataNode = rootRequest.path("metadata");
		ObjectNode jsonResponseNode = jsonResultNode.putObject("response");
		jsonResponseNode.put("uid", jsonmetadataNode.get("uid").asText());
		jsonResponseNode.put("allowed", allowed);
	}

}
