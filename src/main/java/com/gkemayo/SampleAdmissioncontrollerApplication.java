package com.gkemayo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleAdmissioncontrollerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleAdmissioncontrollerApplication.class, args);
	}

}
