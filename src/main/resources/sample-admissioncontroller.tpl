apiVersion: v1
kind: Namespace
metadata:
  name: admctl-ns
---
apiVersion: v1
kind: Secret
metadata:
  name: admctl-secret
  namespace: admctl-ns
stringData:
  keystore_pass: ${KEY_STORE_PASS}
---
apiVersion: v1
kind: Service
metadata:
  name: admctl-svc
  namespace: admctl-ns
  labels:
    name: admctl
spec:
  ports:
  - name: https
    port: 443
    targetPort: 8443
  selector:
    name: admctl 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: admctl-deploy
  namespace: admctl-ns
  labels:
    name: admctl
spec:
  replicas: 1
  selector:
    matchLabels:
      name: admctl
  template:
    metadata:
      name: admctl
      labels:
        name: admctl
    spec:
      containers:
        - name: admctl
          image: gkemayo/kube-admissioncontroller:latest
          imagePullPolicy: Always
          ports:
           - containerPort: 8443
          env:
           - name: SPRING_PROFILES_ACTIVE
             value: prod
           - name: KEY_STORE_PASS
             valueFrom: 
               secretKeyRef:
                 name: admctl-secret
                 key: keystore_pass
          resources:
            requests:
              memory: 250M
              cpu: 100m
            limits:
              memory: 250M
              cpu: 200m
          readinessProbe:
            httpGet:
              scheme: HTTPS
              path: /actuator/health/liveness
              port: 8443
            initialDelaySeconds: 15
            periodSeconds: 45
          livenessProbe:
            httpGet:
              scheme: HTTPS
              path: /actuator/health/readiness
              port: 8443
            initialDelaySeconds: 15
            periodSeconds: 45
---
apiVersion: admissionregistration.k8s.io/v1
kind: ValidatingWebhookConfiguration
metadata:
  name: admctl
webhooks:
  - name: admctl.gkemayo.com
    sideEffects: None
    timeoutSeconds: 30
    admissionReviewVersions: 
      - v1
      - v1beta1
    clientConfig:
      service:
        name: admctl-svc
        namespace: admctl-ns
        path: /validate
        port: 443
      caBundle: ${CA_BUNDLE}
    rules:
      - operations: ["CREATE","UPDATE"]
        apiGroups: ["apps"]
        apiVersions: ["v1"]
        resources: ["deployments"]
        scope: "*"
    #failurePolicy: Ignore