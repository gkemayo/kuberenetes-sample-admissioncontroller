#########################################################
# 1/ Génération des clés ssl CA (Certificate Authority) #
#########################################################

openssl genrsa -out certs/ca.key 2048
openssl req -new -x509 -key certs/ca.key -days 3650 -out certs/ca.crt -config certs/admctl-ca_config.txt

#############################################################################################
# 2/ Génération d'une clé ssl applicative (crt et key) trustée/tamponée par le précédent CA #
#############################################################################################

openssl genrsa -out certs/admctl.key 2048
openssl req -new -key certs/admctl.key -subj "//CN=localhost" -out certs/admctl.csr -config certs/admctl-csr_config.txt
openssl x509 -req -in certs/admctl.csr -CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -days 3650 -out certs/admctl.crt

##########################################################
# 3/ Conversion de la clé ssl applicative en clé pkcs-12 #
##########################################################

openssl pkcs12 -export -in certs/admctl.crt -inkey certs/admctl.key -password pass:pwdtest -name admctl -out certs/admctl.p12

##############################################################
# 4/ Conversion de la clé ssl applicative pkcs-12 en clé jks #
##############################################################

#keytool -importkeystore -srckeystore certs/admctl.p12 -srcstoretype PKCS12 -srcstorepass pwdtest -destkeystore certs/admctl.jks -deststorepass pwdtest -deststoretype JKS




