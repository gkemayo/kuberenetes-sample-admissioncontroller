#########################################################
# Génération des manifests k8s de l'admission controller #
#########################################################

echo "####Exécution de l'admission controller dans k8s####"
export CA_BUNDLE=$(cat certs/ca.crt | base64 | tr -d '\n')
export KEY_STORE_PASS=${VAR_GITLAB_KEY_STORE_PASS}

cat sample-admissioncontroller.tpl | envsubst > sample-admissioncontroller.yaml