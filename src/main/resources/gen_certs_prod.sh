#########################################################
# 1/ Génération des clés ssl CA (Certificate Authority) #
#########################################################

echo "####Génération des clés ssl CA (Certificate Authority)####"
openssl genrsa -out certs/ca.key 2048
openssl req -new -x509 -key certs/ca.key -days 3650 -out certs/ca.crt -config certs/admctl-ca_config.txt

#############################################################################################
# 2/ Génération d'une clé ssl applicative (crt et key) trustée/tamponée par le précédent CA #
#############################################################################################

echo "####Génération d'une clé ssl applicative (crt et key) trustée/tamponée par le précédent CA####"
openssl genrsa -out certs/admctl.key 2048
openssl req -new -key certs/admctl.key -subj "/CN=admctl-svc.admctl-ns.svc" -out certs/admctl.csr -config certs/admctl-csr_config.txt
openssl x509 -req -in certs/admctl.csr -CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -days 3650 -out certs/admctl.crt

##########################################################
# 3/ Conversion de la clé ssl applicative en clé pkcs-12 #
##########################################################

echo "####Conversion de la clé ssl applicative en clé pkcs-12####"
openssl pkcs12 -export -in certs/admctl.crt -inkey certs/admctl.key -password pass:$1 -name admctl -out certs/admctl.p12
